type SubscribeCallback = () => void;

export type Subcriber = {
    key: Symbol;
    callback: SubscribeCallback;
}

export type Unsubcriber = () => void;

export interface IObservable {
    subscribers: Subcriber[]

    subscribe: (callback: SubscribeCallback) => Unsubcriber;
    dispatch: () => void;
}

class Observable implements IObservable {
    subscribers = [] as Subcriber[];

    subscribe(callback: SubscribeCallback) {
        const key: Symbol = Symbol('subscriber')
        this.subscribers.push({key, callback})

        return () => {
            const index = this.subscribers.findIndex(x => x.key = key)
            if (index !== -1) {
                this.subscribers.splice(index, 1)
                return true;
            }

            return false;
        };
    }

    dispatch() {
        for (const {callback} of this.subscribers) {
            callback();
        }
    }
}

export default Observable;