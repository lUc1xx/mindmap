import Canvas from "./Canvas.js";
import Drawable, { IDrawable } from "./Drawable.js";

type Color = string;
type Coordinat = number;

const PADDING: number = 5;

const canvas: HTMLCanvasElement = document.createElement('canvas');
const context: CanvasRenderingContext2D = canvas.getContext("2d") as CanvasRenderingContext2D;
context.font = "48px serif";

interface INodule extends Omit<IDrawable, "draw">{
    textContent: string
    bordered: boolean
    borderColor: Color
    draw: (canvas: Canvas) => void;

}

class Nodule extends Drawable implements INodule{
    textContent = "";
    bordered = true;
    borderColor = "black";

    selectable = true;
    selected = false;

    constructor(textContent?: string, x?: number, y?: number) {
        super();
        if (textContent) this.textContent = textContent;
        if (x !== undefined) this.x = x;
        if (y !== undefined) this.y = y;
    }

    // TODO: 
    // @ts-ignore
    draw(canvas: Canvas) {
        const {context} = canvas;

        context.beginPath();
        context.font = "48px serif";
        context.fillText(this.textContent, this.x, this.y);

        if (this.selectable && this.selected) {
            context.beginPath();
            context.rect(
                this.x - PADDING, 
                this.y - this.height - PADDING,
                this.width + 2 * PADDING,
                this.height + 2 * PADDING
                );
            context.strokeStyle = "blue";
            context.lineWidth = 2;
            context.stroke();
        }

    } 

    get width() {
        const {width} = context.measureText(this.textContent)
        return width;
    }

    get height() {
        return 48;
    }
}

export default Nodule;