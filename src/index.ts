import Application from './Application.js';
import Nodule from './Nodule.js';

const root: HTMLDivElement = document.querySelector('#app') as HTMLDivElement;

const app = new Application(root);

app.add(
    new Nodule("Text", 100, 100),
    new Nodule("Minnullin", -100, -100),
    new Nodule("Dinar"),
);
// app.draw(node);