export interface IDrawable {
    x: number
    y: number
    visible: boolean
    selectable: boolean
    selected: boolean

    draw: (...args: any) => void;

    readonly width: number
    readonly height: number
}

class Drawable implements IDrawable {
    x = 0;
    y = 0;
    visible = true;

    selectable = false;
    selected = false;

    draw(...args: any) {

    }

    get width() {
        return 0;
    }

    get height() {
        return 0;
    }
}

export default Drawable;