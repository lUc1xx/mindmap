import Observable from "./Observable.js";

export enum Key {
    ESC = "escape",

}

type keyState = {
    pressed: boolean;
    fact: boolean;
    prev: boolean;
}

export interface IKeyboard {
    changed: boolean
    done: () => void

    [Key.ESC]: keyState
}

class Keyboard extends Observable implements IKeyboard{
    changed = false;

    [Key.ESC] = {pressed: false, fact: false, prev: false};

    constructor(props) {
        super();
        window.addEventListener('keydown', (e: KeyboardEvent) => {
            if (e.key === Key.ESC) {
                this[Key.ESC].pressed = true;
            }
        })
        window.addEventListener('keyup', (e: KeyboardEvent) => {
            if (e.key === Key.ESC) {
                this[Key.ESC].pressed = false;
            }
        })
    }

    done() {
        if (this.changed) {
            this[Key.ESC].prev = this[Key.ESC].pressed;
        }
    }
}

export default Keyboard; 