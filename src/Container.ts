import Drawable from "./Drawable.js";

type Callback = (element: Drawable, index: number, list: Drawable[]) => void;

export interface IContainer {
    elements: Drawable[]

    add: (...drawables: Drawable[]) => void
    remove: (...drawables: Drawable[]) => void
    forEach: (callback: Callback) => void;
    drawablesFromPoint: (x: number, y: number) => Drawable[]
}

class Container implements IContainer {
    elements = [] as Drawable[];

    add(...drawables: Drawable[]) {
        for (const drawable of drawables) {
            if (!this.elements.includes(drawable)) {
                this.elements.push(drawable);
            }
        }
    }

    remove(...drawables: Drawable[]) {
        this.elements = this.elements.filter((element) => !drawables.includes(element))
    }

    forEach(callback: Callback) {
        this.elements.forEach(callback)
    }

    drawablesFromPoint(x: number, y: number) {
        return this.elements.filter((element) => 
            element.x <= x && 
            x <= element.x + element.width &&
            element.y - element.height <= y &&
            y <= element.y
        )
    }
}

export default Container;